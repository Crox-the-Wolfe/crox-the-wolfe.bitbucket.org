function codeMsg(message, offset, direction) {
        alert("test");
    
        //Values to Change
        var output = "";
        
        //Alphabet array to look up letters and their number in the alphabet
        // Space added in as the start of the array to line up the letter numbers. Arrays start at 0.
        var alphabet = ["","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
        var limit = alphabet.length - 1;
        
        
        //split message into array
        message = message.toLowerCase();    
        var messageArray = message.split("");
        
        for(var letterToLookUp of messageArray) {
            //if the letter we are looking up is in our array then continue
            if(alphabet.includes(letterToLookUp)) {
                //What is the number of the letter to look up?
                var letterToLookUp_Number = alphabet.indexOf(letterToLookUp);

                if(direction == "code") {
                    //Take the letter number and the offset and add them together
                    var newLetterNumber = Number(letterToLookUp_Number) + Number(offset);

                    //if the new letter number is higher than the limit we need to do math
                    if(newLetterNumber > limit) {
                        var modAnswer = (newLetterNumber % limit);
                        newLetterNumber = modAnswer;
                    }

                    //Get the new letter from our alphabet based on the new letter number
                    var newLetter = alphabet[newLetterNumber];
                }

                if(direction == "decode") {
                    //Take the letter number and the offset and add them together
                    var newLetterNumber = Number(letterToLookUp_Number) - Number(offset);

                    //if the new letter number is less than 0 we need to add 26
                    if(newLetterNumber < 1) {
                        newLetterNumber = Number(newLetterNumber) + 26;
                    }

                    //Get the new letter from our alphabet based on the new letter number
                    var newLetter = alphabet[newLetterNumber];
                }
            } else {
                //we don't know what the letterToLookUp is so make it a space
                var newLetter = " ";
            }

            //Add new letter to our output
            output += newLetter;
        }
        
        return output;
    }